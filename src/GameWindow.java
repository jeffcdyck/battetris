import Game.Board;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 07/06/14
 * Time: 1:35 AM
 * To change this template use File | Settings | File Templates.
 */
public class GameWindow extends JFrame
{
    private JPanel MainPanel;
    private JPanel panelTopInfo;
    private JPanel panelGameplayArea;
    private JPanel panelPlayer1;
    private JPanel panelMiddleGameplay;
    private JPanel panelPlayer2;
    private JPanel panelTopInfoLeft;
    private JPanel panelTopInfoRight;
    private JLabel lbPlayer1Name;
    private JLabel lbPlayer2Name;
    private JLabel lbPlayer1Score;
    private JLabel lbPlayer2Score;

    static final int WINDOW_WIDTH = 1440;
    static final int WINDOW_HEIGHT = 900;

    Controller controller;
    Model model;

    public GameWindow(Controller c, Model m)
    {
        super();

        controller = c;
        model = m;

        pack();
        setSize(1440, 900);
        setContentPane(MainPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(new MyDispatcher());

        setVisible(true);
    }

    private void createUIComponents()
    {
        panelPlayer1 = new GamePanel("foo", model.getPlayer1());
        panelPlayer2 = new GamePanel("bar", model.getPlayer2());
    }

    private class MyDispatcher implements KeyEventDispatcher
    {
        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            controller.handleKeyPress(e);
            return false;
        }
    }

    private class GamePanel extends JPanel
    {
        Board player;
        String name;

        public GamePanel(String n, Board b)
        {
            super();
            System.out.println("Created gamepanel");
            name = n;
            player = b;
            setBackground(Color.blue);
        }

        @Override
        public void paintComponent(Graphics g)
        {
            super.paintComponent(g);
            System.out.println("Painting " + name);
            player.draw(g);
        }
    }
}
