import Game.BattetrisGame;
import Game.Board;

/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 06/06/14
 * Time: 4:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class Model
{
    BattetrisGame game;
    public Model()
    {
        game = new BattetrisGame();
    }

    public BattetrisGame getGame() { return game; }
    public Board getPlayer1() { return game.getBoardPlayer1(); }
    public Board getPlayer2() { return game.getBoardPlayer2(); }
}
