import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 06/06/14
 * Time: 5:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class Main
{
    public static void main(String[] args)
    {
        int FPS = 30;

        Model model = new Model();
        Controller controller = new Controller(model);
        // View view = new View(controller, model);
        final GameWindow gw = new GameWindow(controller, model);

        Timer drawTimer;
        drawTimer = new Timer(1000/FPS, new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                // gw.repaint();
            }
        });

        drawTimer.start();
    }
}
