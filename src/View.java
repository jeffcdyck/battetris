import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 06/06/14
 * Time: 4:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class View
{
    static final int WINDOW_WIDTH = 1440;
    static final int WINDOW_HEIGHT = 900;

    Controller controller;
    Model model;

    public View(Controller c, Model m)
    {
        controller = c;
        model = m;
    }
}
