package Game;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 06/06/14
 * Time: 4:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class Board
{
    static final int GRID_WIDTH = 10;
    static final int GRID_HEIGHT = 20;

    static final int BLOCK_SIZE = 20;

    Pieces.PiecesTypes grid[][];

    int score;
    Powerup powerup;
    private String playerName;

    public Board()
    {
        reInitializeGrid();
        score = 0;

        powerup = null;
    }

    public void reInitializeGrid()
    {
        grid = new Pieces.PiecesTypes[GRID_HEIGHT][GRID_WIDTH];
        for (int i = 0; i < GRID_HEIGHT; i++)
        {
            for (int j = 0; j < GRID_WIDTH; j++)
            {
                grid[i][j] = Pieces.PiecesTypes.NONE;
            }
        }

        grid[0][0] = Pieces.PiecesTypes.I_SHAPE;
        grid[1][0] = Pieces.PiecesTypes.J_SHAPE;
        grid[2][0] = Pieces.PiecesTypes.S_SHAPE;
        grid[4][1] = Pieces.PiecesTypes.Z_SHAPE;
        grid[4][2] = Pieces.PiecesTypes.L_SHAPE;
    }

    public void draw(Graphics g)
    {
        for (int i = 0; i < GRID_HEIGHT; i++)
        {
            for (int j = 0; j < GRID_WIDTH; j++)
            {
                Pieces.PiecesTypes type = grid[i][j];
                if (type != Pieces.PiecesTypes.NONE)
                {
                    g.setColor(Pieces.PiecesColours[type.ordinal()]);
                    g.fillRect(BLOCK_SIZE*i, BLOCK_SIZE*j, BLOCK_SIZE, BLOCK_SIZE);
                }
            }
        }
    }

    public int getScore() {return score;}
    public String getPlayerName() {return playerName;}

    public void affectByPowerup(Powerup p)
    {
    }
}
