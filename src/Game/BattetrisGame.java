package Game;

/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 06/06/14
 * Time: 4:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class BattetrisGame
{
    Board boards[];

    public BattetrisGame()
    {
        boards = new Board[2];
        boards[0] = new Board();
        boards[1] = new Board();
    }

    public Board getBoardPlayer1()
    {
        return boards[0];
    }

    public Board getBoardPlayer2()
    {
        return boards[1];
    }
}
