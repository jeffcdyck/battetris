package Game;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 08/06/14
 * Time: 2:10 AM
 * To change this template use File | Settings | File Templates.
 */
public class Pieces
{
    public static Color[] PiecesColours = new Color[]
    {
        Color.CYAN, Color.BLUE, Color.ORANGE, Color.YELLOW,
        Color.RED, Color.MAGENTA, Color.GREEN,
        Color.WHITE
    };

    public static enum PiecesTypes
    {
        I_SHAPE, J_SHAPE, L_SHAPE, O_SHAPE,
        Z_SHAPE,  T_SHAPE, S_SHAPE,
        NONE
    }

    public static int[][][][] PiecesShapes =
    {
            {
                {
                    {0, 0, 0, 0},
                    {1, 1, 1, 1},
                    {0, 0, 0, 0},
                    {0, 0, 0, 0}
                },
                {
                    {0, 0, 1, 0},
                    {0, 0, 1, 0},
                    {0, 0, 1, 0},
                    {0, 0, 1, 0}
                },
                {
                    {0, 0, 0, 0},
                    {0, 0, 0, 0},
                    {1, 1, 1, 1},
                    {0, 0, 0, 0}
                },
                {
                    {0, 1, 0, 0},
                    {0, 1, 0, 0},
                    {0, 1, 0, 0},
                    {0, 1, 0, 0}
                }
            },
            {
                {
                    {1, 0, 0},
                    {1, 1, 1},
                    {0, 0, 0}
                },
                {
                    {0, 1, 1},
                    {0, 1, 0},
                    {0, 1, 0}
                },
                {
                    {0, 0, 0},
                    {1, 1, 1},
                    {0, 0, 1}
                },
                {
                    {0, 1, 0},
                    {0, 1, 0},
                    {1, 1, 0}
                },
            },
            {
                {
                    {0, 0, 1},
                    {1, 1, 1},
                    {0, 0, 0}
                },
                {
                    {0, 1, 0},
                    {0, 1, 0},
                    {0, 1, 1}
                },
                {
                    {0, 0, 0},
                    {1, 1, 1},
                    {1, 0, 0}
                },
                {
                    {1, 1, 0},
                    {0, 1, 0},
                    {0, 1, 0}
                }
            },
            {
                {
                    {1, 1},
                    {1, 1}
                },
                {
                    {1, 1},
                    {1, 1}
                },
                {
                    {1, 1},
                    {1, 1}
                },
                {
                    {1, 1},
                    {1, 1}
                },
            },
            {
                {
                    {1, 1, 0},
                    {0, 1, 1},
                    {0, 0, 0}
                },
                {
                    {0, 0, 1},
                    {0, 1, 1},
                    {0, 1, 0}
                },
                {
                    {0, 0, 0},
                    {1, 1, 0},
                    {0, 1, 1}
                },
                {
                    {0, 1, 0},
                    {1, 1, 0},
                    {1, 0, 0}
                }
            },
            {
                {
                    {0, 1, 0},
                    {1, 1, 1},
                    {0, 0, 0}
                },
                {
                    {0, 1, 0},
                    {0, 1, 1},
                    {0, 1, 0}
                },
                {
                    {0, 0, 0},
                    {1, 1, 1},
                    {0, 1, 0}
                },
                {
                    {0, 1, 0},
                    {1, 1, 0},
                    {0, 1, 0}
                }
            },
            {
                {
                    {0, 1, 1},
                    {1, 1, 0},
                    {0, 0, 0}
                },
                {
                    {0, 1, 0},
                    {0, 1, 1},
                    {0, 0, 1}
                },
                {
                    {0, 0, 0},
                    {0, 1, 1},
                    {1, 1, 0}
                },
                {
                    {1, 0, 0},
                    {1, 1, 0},
                    {0, 1, 0}
                }
            }
    };

    public PiecesTypes type;
    public int[][] pieceGrid;
    public int rotation;

    public Pieces(PiecesTypes t)
    {
        type = t;
        rotation = 0;
        updateBlock();
    }

    private void updateBlock()
    {
        pieceGrid = PiecesShapes[type.ordinal()][rotation].clone();
    }

    public void rotateCW()
    {
        if (rotation == 3) rotation = 0;
        else rotation++;
        updateBlock();
    }
    public void rotateCCW()
    {
        if (rotation == 0) rotation = 3;
        else rotation--;
        updateBlock();
    }
}
