import java.awt.event.KeyEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Jeff
 * Date: 06/06/14
 * Time: 4:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class Controller
{
    Model model;

    public Controller(Model m)
    {
        model = m;

    }

    public void handleKeyPress(KeyEvent e)
    {
        System.out.println("Got event " + e.getKeyChar());
    }
}
